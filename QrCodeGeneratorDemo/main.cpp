
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "qrcodegen.hpp"

static const cv::Vec3b kCVColorLight(0, 0, 0);
static const cv::Vec3b kCVColorDark(255, 255, 255);

static bool toImage(const qrcodegen::QrCode& qr, int scale, int border, cv::Mat& result) {
	if (scale <= 0 || border < 0) {
		std::cout << "Value out of range" << std::endl;
		return false;
	}
	if (border > INT_MAX / 2 || qr.getSize() + border * 2L > INT_MAX / scale) {
		std::cout << "Scale or border too large" << std::endl;
		return false;
	}

	cv::Mat image((qr.getSize() + border * 2) * scale, (qr.getSize() + border * 2) * scale, CV_8UC3);
	for (int y = 0; y < image.cols; y++) {
		for (int x = 0; x < image.rows; x++) {
			bool color = qr.getModule(x / scale - border, y / scale - border);
			if (color)
			{
				image.at<cv::Vec3b>(x, y) = kCVColorLight;
			}
			else
			{
				image.at<cv::Vec3b>(x, y) = kCVColorDark;
			}
		}
	}

	image.copyTo(result);
	return true;
}

int main() {
	const std::string text = "Hello, world!";              // User-supplied text
	const qrcodegen::QrCode::Ecc errCorLvl = qrcodegen::QrCode::Ecc::MEDIUM;  // Error correction level
	const int version = 10;
	// Make and print the QR Code symbol
	std::vector<qrcodegen::QrSegment> segs = qrcodegen::QrSegment::makeSegments(text.c_str());
	const qrcodegen::QrCode qr = qrcodegen::QrCode::encodeSegments(segs, errCorLvl, version, version, -1, true);

	cv::Mat result;
	if (!toImage(qr, 10, 2, result))
	{
		std::cout << "Failed to generate image!" << std::endl;
		return -1;
	}

	cv::Mat logo = cv::imread("logo.png", cv::IMREAD_ANYCOLOR);
	if (logo.empty())
	{
		std::cout << "logo image is empty!" << std::endl;
		return -1;
	}

	// ����roi
	cv::Rect roi_rect = cv::Rect(result.cols / 2 - logo.cols / 2, result.rows / 2 - logo.rows / 2, logo.cols, logo.rows);
	logo.copyTo(result(roi_rect));

	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(3);

	try {
		cv::imwrite("test.png", result, compression_params);
	}
	catch (const std::exception& ex) {
		std::cout << "Exception converting image to PNG format: " << ex.what();
		return 1;
	}
	std::cout << "Saved PNG file with alpha data.";
	return 0;
}